<?php

use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/{url}', [DashboardController::class, 'index'])->where('url', '(/|dashboard|home)');
Route::view('{url}', 'admin.dashboard')->where('url', '(dashboard||home)');
Route::resource("user", UserController::class);
Route::resource("role", RoleController::class);

Route::view('post_category', 'admin.post_category');
Route::view('post', 'admin.post');
