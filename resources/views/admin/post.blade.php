@extends('admin.layouts.app')

@section('meta-title','Post')
@section('topbar-name','Post')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Add Post</h4>
                        <p class="text-muted m-b-30 font-14">You can add Post and assign role to particular Post</p>

                        <form class="" action="#">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" required placeholder="Title"/>
                            </div>

                            <div class="form-group">
                                <label>Meta Title</label>
                                <input type="text" class="form-control" required placeholder="Meta Title"/>
                            </div>

                            <div class="form-group">
                                <label>Category</label>
                                <div>
                                    <select class="custom-select">
                                        <option>-- Select Category --</option>
                                        <option value="1">Sensex</option>
                                        <option value="2">Business</option>
                                        <option value="3">Political</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Tags</label>
                                <input type="text" class="form-control" required placeholder="Separate tags with commas"/>
                            </div>

                            <div class="form-group">
                                <label>Publish Status</label>
                                <div>
                                    <select class="custom-select">
                                        <option>-- Select Publish Status  --</option>
                                        <option value="1" selected>Draft</option>
                                        <option value="2">Private</option>
                                        <option value="3">Published</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Featured Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Summary</label>
                                <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars."></textarea>
                            </div>

                            <div class="form-group">
                                <label>Content</label>
                                <textarea id="elm1" name="area"></textarea>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-pink waves-effect waves-light m-r-5">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Post List</h4>
                        <p class="text-muted m-b-30 font-14">
                            You can edit-role to the Post
                        </p>

                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Post Title</th>
                                    <th>Category</th>
                                    <th>Author</th>
                                    <th>Last Modified Date</th>
                                    <th>Publish Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>28-year-old woman set to become new Chennai Mayor</td>
                                    <td>Trending</td>
                                    <td>Kaniyan</td>
                                    <td>11,Feb 2022</td>
                                    <td>Draft</td>
                                    <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Tamil Nadu seeks Kerala nod for repair of Mullaperiyar dam</td>
                                    <td>Trending</td>
                                    <td>Brindha</td>
                                    <td>1,Mar 2022</td>
                                    <td>Draft</td>
                                    <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>Aavin increases prices of curd, ghee and other milk products</td>
                                    <td>City News</td>
                                    <td>Brindha</td>
                                    <td>4,Mar 2022</td>
                                    <td>Published</td>
                                    <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
@endsection('main-content')


@section('page-dependent-scripts')
<!-- START Plugins textarea js=>textarea lenght find 1/255-->
<script src="admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js"></script>
<script src="admin/assets/pages/form-advanced.js"></script>
<!-- END textarea Plugins js=>textarea lenght find 1/255 -->

<!-- START Plugins for Editor-->
<!--Wysiwig js-->
<script src="admin/plugins/tinymce/tinymce.min.js"></script>
<script>
    $(document).ready(function () {
        if($("#elm1").length > 0){
            tinymce.init({
                selector: "textarea#elm1",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
    });
</script>
<!-- END Plugins for Editor-->
@endsection('page-dependent-scripts')
