<footer class="footer">
         © <?php echo date("Y",strtotime("-1 year")); ?> - <?php echo date("Y"); ?> <i class="mdi mdi-heart text-danger"></i> {{Config::get('app.name')}} <span class="text-muted d-none d-sm-inline-block float-right">All rights reserved</span>
</footer>