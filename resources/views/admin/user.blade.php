@extends('admin.layouts.app')

@section('meta-title','User')
@section('topbar-name','User')

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-5">
            <div class="card m-b-20">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add User</h4>
                    <p class="text-muted m-b-30 font-14">You can add user and assign role to particular user</p>

                    <form class="" action="#">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" required placeholder="First Name"/>
                        </div>

                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" required placeholder="Last Name"/>
                        </div>

                        <div class="form-group">
                            <label>E-Mail</label>
                            <div>
                                <input type="email" class="form-control" required parsley-type="email" placeholder="Enter a valid e-mail"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <div>
                                <input type="password" id="pass2" class="form-control" required placeholder="Password"/>
                            </div>
                            <div class="m-t-10">
                                <input type="password" class="form-control" required data-parsley-equalto="#pass2" placeholder="Re-Type Password"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Select Role</label>
                            <div>
                                <select class="custom-select">
                                    <option selected>-- Select role --</option>
                                    <option value="1">Writer</option>
                                    <option value="2">Editor</option>
                                    <option value="3">Publisher</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-pink waves-effect waves-light m-r-5">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->

        <div class="col-lg-7">
            <div class="card m-b-20">
                <div class="card-body">

                    <h4 class="mt-0 header-title">User List</h4>
                    <p class="text-muted m-b-30 font-14">
                        You can edit-role to the user
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>mark@gmail.com</td>
                                <td>Editor</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>jacob@gmail.com</td>
                                <td>Writer</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>Larry@gmail.com</td>
                                <td>Publisher</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

</div><!-- container -->
@endsection('main-content')
