@extends('admin.layouts.app')

@section('meta-title','Post Category')
@section('topbar-name','Post Category')

@section('main-content')
    <div class="container-fluid">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Add Post Category</h4>
                        <p class="text-muted m-b-30 font-14">You can add more sub category </p>

                        <form class="" action="#">
                            <div class="form-group">
                                <label>Main Category</label>
                                <input type="text" class="form-control" required placeholder=""/>
                            </div>

                            <div class="form-group bs-form-wrapper">
                                <label>Sub Category</label>
                                <div class="input-group control-group input-wrapper">  
                                    <input type="text" name="name[]" class="form-control" placeholder="Enter sub category here">  
                                    <div class="input-group-btn">   
                                        <button class="btn btn-success bs-add-button" type="button"><i class="fa fa-plus"></i> Add</button>  
                                    </div>  
                                </div>  
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-pink waves-effect waves-light m-r-5">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

            <div class="col-lg-6">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Post Category List</h4>
                        <p class="text-muted m-b-30 font-14">
                            You can edit sub category here
                        </p>

                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Main Category</th>
                                    <th>Author</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Trending</td>
                                        <td>Kaniyan</td>
                                        <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Breaking News</td>
                                        <td>Brindha</td>
                                        <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
        
    </div><!-- container -->
@endsection('main-content')

@section('page-dependent-scripts')

<script type="text/javascript">
    $(document).ready(function () {
	    var maxLimit = 5;
	    var appendHTML = '<div class="input-group control-group input-wrapper mt-2"><input type="text" name="name[]" class="form-control" placeholder="Enter value here"><div class="input-group-btn"><button class="btn btn-danger bs-remove-button" type="button"><i class="fa fa-minus"></i> Remove</button></div></div>'; 
	    var x = 1;
	    
	    // for addition
	    $('.bs-add-button').click(function(e){
	    	e.preventDefault();
	        // if(x < maxLimit){ 
	            $('.bs-form-wrapper').append(appendHTML);
	            x++;
	        // }
	    });
	    
	    // for deletion
	    $('.bs-form-wrapper').on('click', '.bs-remove-button', function(e){
	        e.preventDefault();
	        $(this).parents('.input-wrapper').remove();
	        x--;
	    });
	});

</script>
@endsection('page-dependent-scripts')
