@extends('admin.layouts.app')

@section('meta-title','Role')
@section('topbar-name','Role')

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6">
            <div class="card m-b-20">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add Role</h4>
                    <p class="text-muted m-b-30 font-14">You can add one or more permissions to the role</p>

                    <form class="" action="#">
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" class="form-control" required placeholder="First Name"/>
                        </div>

                        <div class="form-group">
                            <label>Permissions</label>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="permission_checkbox1" value="option1">
                                <label class="custom-control-label font-weight-normal" for="permission_checkbox1">Create Post</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="permission_checkbox2" value="option2">
                                <label class="custom-control-label font-weight-normal" for="permission_checkbox2">Edit Post</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="permission_checkbox3" value="option3">
                                <label class="custom-control-label font-weight-normal" for="permission_checkbox3">Publish Post</label>
                            </div>
                        </div>  

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-pink waves-effect waves-light m-r-5">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->

        <div class="col-lg-6">
            <div class="card m-b-20">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Role List</h4>
                    <p class="text-muted m-b-30 font-14">
                        You can edit-permissions for the role.
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Role Name</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Admin</td>
                                <td>Create Post, Edit Post, Publish Post</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Writer</td>
                                <td>Create Post</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Editor</td>
                                <td>Edit Post</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>Publisher</td>
                                <td>Publish Post</td>
                                <td><a href="#" class="text-success"><i class="fa fa-pencil-square-o"></i></a> / <a href="#" class="text-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div><!-- container -->
@endsection('main-content')
